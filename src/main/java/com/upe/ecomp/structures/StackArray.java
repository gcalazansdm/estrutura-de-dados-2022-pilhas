package com.upe.ecomp.structures;

public class StackArray implements Stack {

    private String[] innerStack = new String[15];
    private int counter = 0;

    public void push(String string){
        if(counter >= innerStack.length){
            String[] newStack = new String[counter * 2];
            System.arraycopy(innerStack, 0, newStack, 0, innerStack.length);
            innerStack = newStack;
        }
        innerStack[counter] = string;
        counter++;
    }

    public String pop(){
        if(isEmpty())
            throw new RuntimeException();
        counter--; // recua o counter
        String value = innerStack[counter]; // Pego a posição do elemento do topo
        innerStack[counter] = null; // Limpa memoria
        return value;
    }

    public int size(){
        return counter;
    }

    public boolean isEmpty(){
        return counter == 0;
    }

    public String top(){
        if(isEmpty())
            return null;
        return innerStack[counter -1];
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        for(int i = counter -1; i >=0 ; i--){
            s.append(innerStack[i]);
        }
        return s.toString();
    }
}
