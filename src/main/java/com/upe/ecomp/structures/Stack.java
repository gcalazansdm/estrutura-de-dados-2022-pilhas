package com.upe.ecomp.structures;

public interface Stack {
    void push(String string);

    String pop();

    boolean isEmpty();

    int size();

    String top();
}
