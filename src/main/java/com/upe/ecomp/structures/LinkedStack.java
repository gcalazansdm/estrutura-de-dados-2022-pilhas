package com.upe.ecomp.structures;

public class LinkedStack implements Stack {

    private Node top = null;
    private int size = 0;

    @Override
    public void push(String string){
        if(top == null){
            top = new Node(string);
            return;
        }
        Node oldTop = top;
        top = new Node(string);
        top.setNext(oldTop);
        size++;
    }
    @Override
    public String pop(){
        if(isEmpty())
            throw new RuntimeException();
        String rValue = top.getValue();
        top = top.getNext();
        //Delete old top;
        // com.upe.ecomp.structures.Node* oldtop = top
        // top = top->next
        // free(oldtop)
        size--;
        return rValue;
    }
    @Override
    public boolean isEmpty(){
        return top == null;
    }

    @Override
    public int size(){
        return size--;
    }

    @Override
    public String top(){
        return top.getValue();
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        for(Node aux = top; aux != null; aux = aux.getNext()){
            s.append(aux.getValue()).append("\n");
        }
        return s.toString();
    }
}
