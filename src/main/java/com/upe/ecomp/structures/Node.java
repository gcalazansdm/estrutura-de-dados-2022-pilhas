package com.upe.ecomp.structures;

public class Node {
    private String value;
    private Node next;

    public Node(String value){
        this.value = value;
    }

    public Node getNext() {
        return next;
    }

    public String getValue() {
        return value;
    }

    public void setNext(Node next) {
        this.next = next;
    }
}
