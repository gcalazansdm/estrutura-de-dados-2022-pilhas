package com.upe.ecomp.exercices;

import com.upe.ecomp.structures.Stack;
import com.upe.ecomp.structures.StackArray;

public class ToBinary {

    public static Stack toBinary(int number) {
        Stack s = new StackArray();
        int aux = number;
        while (aux >= 2) {
            s.push("" + aux % 2);
            aux = aux / 2;
        }
        s.push("" + aux);
        return s;
    }

    public static String toBinaryRec(int number) {
        if (number < 2)
            return number + "";

        return toBinaryRec(number / 2) + number % 2;
    }

    public static void main(String[] args) {
        int[] numbers = new int[]{1, 2, 5, 15, 97, 555555555};
        for (int number : numbers) {
            System.out.print(number + " -> " +
                    ToBinary.toBinary(number) + " " +
                    ToBinary.toBinaryRec(number));
        }
    }
}
