package com.upe.ecomp.exercices;

import com.upe.ecomp.structures.LinkedStack;
import com.upe.ecomp.structures.Stack;

public class Main {


    public static void main(String[] args) {
        Stack s = new LinkedStack();

        System.out.println(s.isEmpty());

        s.push ("A");
        s.push ("B");
        s.push ("C");
        s.push ("D");
        s.push ("E");
        s.push ("F");
        s.push ("G");
        s.push ("H");
        s.push ("I");
        s.push ("J");

        System.out.println(s);

        System.out.println("Removi = "+ s.pop());
        System.out.println("Removi = "+ s.pop());
        System.out.println("Removi = "+ s.pop());
        System.out.println("Removi = "+ s.pop());
        System.out.println("Removi = "+ s.pop());
        System.out.println("Removi = "+ s.pop());

        s.push("K");

        System.out.println("Removi = "+ s.pop());
        System.out.println("Removi = "+ s.pop());
        System.out.println("Removi = "+ s.pop());

        s.push ("G");

        System.out.println(s);
        System.out.println("top = "+ s.top());
        System.out.println(s.isEmpty());
        System.out.println(s);

        s.pop();
        s.pop();
        s.pop();

        System.out.println(s);
        System.out.println(s.isEmpty());
    }
}
