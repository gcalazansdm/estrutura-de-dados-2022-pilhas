package com.upe.ecomp.exercices;

import com.upe.ecomp.structures.Stack;
import com.upe.ecomp.structures.StackArray;

public class InvertStack {

    public static void invert(Stack stackA) {
        StackArray stackB = new StackArray();
        StackArray stackC = new StackArray();

        int count = stackA.size();

        while (count > 0) {
            stackB.push(stackA.pop());
            for (int i = 1; i < count; i++) {
                stackC.push(stackA.pop());
            }

            stackA.push(stackB.pop());

            while (!stackC.isEmpty()) {
                stackA.push(stackC.pop());
            }

            count--;
        }
    }

    public static void main(String[] args) {
        int[] numbers = new int[]{1, 2, 5, 15, 97, 555555555};
        for (int number : numbers) {
            Stack stack = ToBinary.toBinary(number);

            System.out.print(number + " -> " + stack + " ");
            invert(stack);

            System.out.println(stack);

        }
    }
}
